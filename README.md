# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository will contain files that will be used for my research.

### Instructions for scheldwoorden.sh (retrieve data for this research) ###

Run the script within the repository on the karora server, make sure that the desired files are there:
	-	Twitter corpus of the university of Groningen
	-	cursewords.txt
	-	get-gender.py
	-	names/

If you wish to change the date of the script, simply alter the date at the gunzip -c /net/corpora/twitter2/Tweets/{year}/{month}/{date}{time}* part of the script.
