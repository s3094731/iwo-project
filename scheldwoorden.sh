#!/bin/bash
# In dit script zullen een aantal scheldwoorden worden gefilterd
# Ditscript gebruikt de scheldwoorden uit cursewords.txt 

echo 'This script will read the input from twitter data of the 1st of March 2017, and cursewords.txt.'
echo 'At the end it will return the amount of cursewords used by male and female.'
echo 'Processing...'

gunzip -c /net/corpora/twitter2/Tweets/2017/03/20170305* | /net/corpora/twitter2/tools/tweet2tab -i user text > twitterdata.txt 
file='cursewords.txt'
while read p; do
        cat twitterdata.txt | grep $p >> cursetweets.txt
done < $file

cat cursetweets.txt | sort | uniq > cursetweets
zless cursetweets | python get-gender.py > cursetweetsgenders.txt
cat cursetweetsgenders.txt | grep -i '^male' > cursetweetsmale.txt
cat cursetweetsgenders.txt | grep -i '^female' > cursetweetsfemale.txt

echo 'The total amount of tweets:'
cat twitterdata.txt | wc -l
echo 'The amount of tweets containing curses (without duplicates): '
cat cursetweets | wc -l
echo 'The amount of males that used cursewords in this data are:'
cat cursetweetsmale.txt | wc -l
echo 'The amount of females that used cursewords in this data are:'
cat cursetweetsfemale.txt | wc -l

echo 'Deleting created files...'
rm twitterdata.txt
rm cursetweets.txt
rm cursetweets
rm cursetweetsgenders.txt
rm cursetweetsmale.txt
rm cursetweetsfemale.txt

echo 'Done.'
