#bin/bash
echo 'This script will look in the Twitter corpus of the university of Groningen and takes the tweets of March 1st 12:00 till 13:00.'
echo 'The amount of tweets in this sample: ' 

#First we gunzip the file, we use the tool tweet2tab that takes out the wanted data from the file, after that we count the lines (each line represent one tweet)

gunzip -c /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i user text | wc -l
echo 'The amount of unique tweets: '
#The same as the first command except we sort it, and we use the command uniq to throw out the tweets that appear twice or more
gunzip -c /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i user text | sort | uniq | wc -l
echo 'The amount of retweets: '
#Here we do the same as the command above and we grep all the lines that start with RT, which stands for Retweet
gunzip -c /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l
echo 'The first 20 tweets are the following: '
#Here we want all the lines that DONT contains RT and then we say head -20 so that the first 20 tweets will appear

gunzip -c /net/corpora/twitter2/Tweets/2017/03/20170301:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v '^RT' | head -20
